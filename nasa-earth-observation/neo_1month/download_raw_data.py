#!/usr/bin/env python3
"""Downloads the nasa grayscale data for monthly representations."""
from pathlib import Path
from multiprocessing import Pool, cpu_count
from argparse import ArgumentParser
from loguru import logger
import requests
import shutil
from bs4 import BeautifulSoup
from tqdm import tqdm
import pandas as pd

base_url = "https://neo.gsfc.nasa.gov/archive/gs"

"""mapping between our names and the names in the nasa urls"""
urls = [
    ("AOD", "MODAL2_M_AER_OD"),
    ("CHLORA", "MY1DMM_CHLORA"),
    ("CLD_FR", "MYDAL2_M_CLD_FR"),
    ("CLD_RD", "MODAL2_M_CLD_RD"),
    ("CLD_WP", "MYDAL2_M_CLD_WP"),
    ("COT", "MODAL2_M_CLD_OT"),
    ("FIRE", "MOD14A1_M_FIRE"),
    ("LAI", "MOD15A2_M_LAI"),
    ("LSTD_AN", "MOD_LSTAD_M"),
    ("LSTD", "MOD_LSTD_M"),
    ("LSTN_AN", "MOD_LSTAN_M"),
    ("LSTN", "MOD_LSTN_M"),
    ("NO2", "AURA_NO2_M"),
    ("OZONE", "AURA_OZONE_M"),
    ("SNOWC", "MOD10C1_M_SNOW"),
    ("SST", "MYD28M"),
    ("WV", "MODAL2_M_SKY_WV"),
    ("NDVI", "MOD_NDVI_M"),
    ("CO_M", "MOP_CO_M")
]

def crawl_data(url_one_repr: str) -> list[str]:
    """gets the image links from the NEO website"""
    req = requests.get(url_one_repr, allow_redirects=True)
    bs_html = BeautifulSoup(req.content, features="html.parser")
    all_a = bs_html.find_all("a")
    all_a_png = [str(x) for x in all_a if str(x).find("PNG") != -1]
    all_png = [a_png.split("\">")[1][0:-4] for a_png in all_a_png]
    return all_png

def get_url_to_particle_csv() -> pd.DataFrame:
    """
    creates a csv, for each link of the format link, node_type, week or month
    Example:
    https://neo.gsfc.nasa.gov/archive/gs/MOD_LSTN_M/MOD_LSTN_M_2010-05.PNG,LSTN,2010-05
    """
    url_to_particle = []
    for name, url_name in urls:
        url_one_repr = f"{base_url}/{url_name}"
        all_png = crawl_data(url_one_repr)
        assert len(all_png) > 0, name
        logger.debug(f"Node '{name}' from url '{url_one_repr}'. Got {len(all_png)} image urls.")
        for png_name in all_png:
            # AURA_NO2_E_2005-01-17.PNG -> 2005-01-17
            particle = png_name.split("_")[-1].split(".PNG")[0]
            image_link = f"{url_one_repr}/{png_name}"
            url_to_particle.append([image_link, name, particle])
    url_to_out_file = pd.DataFrame(url_to_particle, columns=["link", "node", "particle"])
    return url_to_out_file

def do_one(x):
    in_url: str = x[0]
    out_file = Path(x[1])
    out_file.parent.mkdir(exist_ok=True, parents=True)
    if out_file.exists():
        return
    req = requests.get(in_url, allow_redirects=True, stream=True)
    with open(out_file, "wb") as fp:
        shutil.copyfileobj(req.raw, fp)
    del req

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--output_path", "-o", required=True, type=Path)
    parser.add_argument("--n_cores", type=int, default=-1)
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    args.output_path.mkdir(exist_ok=True, parents=True)
    logger.info(f"[NASA 1 month] Getting data from '{base_url}'. Storing to '{args.output_path}'. N repr: {len(urls)}")

    url_to_particle_file = Path(f"{args.output_path}/url_to_particle.csv")
    if not url_to_particle_file.exists():
        logger.info(f"URL to out file CSV doesn't exist. Crawling and generating to '{url_to_particle_file}'")
        get_url_to_particle_csv().to_csv(url_to_particle_file)
    df = pd.read_csv(url_to_particle_file, index_col=0)
    logger.info(f"Loaded CSV from '{url_to_particle_file}'. Total data to download: {len(df)}")

    in_files = df.link
    out_files = (args.output_path) / df.apply(lambda x: f"{x.node}/{x.particle}.png", axis=1)
    data = list(zip(in_files, out_files))
    map_fn = map if args.n_cores == 0 else (Pool(cpu_count() if args.n_cores == -1 else args.n_cores).imap)
    list(map_fn(do_one, tqdm(data)))
    logger.info("Done!")

if __name__ == "__main__":
    main()
