#!/usr/bin/env python3
"""script that takes the full dataset and splits it into the train and validation sets of the ICCV23 paper"""
from pathlib import Path
from argparse import ArgumentParser, Namespace
from loguru import logger
import shutil
from tqdm import tqdm

train_data = ["2004-10.npz", "2004-11.npz", "2004-12.npz", "2005-01.npz", "2005-02.npz", "2005-03.npz", "2005-04.npz",
              "2005-05.npz", "2005-06.npz", "2005-07.npz", "2005-08.npz", "2005-09.npz", "2005-10.npz", "2005-11.npz",
              "2005-12.npz", "2006-01.npz", "2006-02.npz", "2006-03.npz", "2006-04.npz", "2006-05.npz", "2006-06.npz",
              "2006-07.npz", "2006-08.npz", "2006-09.npz", "2006-10.npz", "2006-11.npz", "2006-12.npz", "2007-01.npz",
              "2007-02.npz", "2007-03.npz", "2007-04.npz", "2007-05.npz", "2007-06.npz", "2007-07.npz", "2007-08.npz",
              "2007-09.npz", "2007-10.npz", "2007-11.npz", "2007-12.npz", "2008-01.npz", "2008-02.npz", "2008-03.npz",
              "2008-04.npz", "2008-05.npz", "2008-06.npz", "2008-07.npz", "2008-08.npz", "2008-09.npz", "2008-10.npz",
              "2008-11.npz", "2008-12.npz", "2009-01.npz", "2009-02.npz", "2009-03.npz", "2009-04.npz", "2009-05.npz",
              "2009-06.npz", "2009-07.npz", "2009-08.npz", "2009-09.npz", "2009-10.npz", "2009-11.npz", "2009-12.npz",
              "2010-01.npz", "2010-02.npz", "2010-03.npz", "2010-04.npz", "2010-05.npz", "2010-06.npz", "2010-07.npz",
              "2010-08.npz", "2010-09.npz", "2010-10.npz", "2010-11.npz", "2010-12.npz", "2011-01.npz", "2011-02.npz",
              "2011-03.npz", "2011-04.npz", "2011-05.npz", "2011-06.npz", "2011-07.npz", "2011-08.npz", "2011-09.npz",
              "2011-10.npz", "2011-11.npz", "2011-12.npz", "2012-01.npz", "2012-02.npz", "2012-03.npz", "2012-04.npz",
              "2012-05.npz", "2012-06.npz", "2012-07.npz", "2012-08.npz", "2012-09.npz", "2012-10.npz", "2012-11.npz",
              "2012-12.npz", "2013-01.npz", "2013-02.npz", "2013-03.npz", "2013-04.npz", "2013-05.npz", "2013-06.npz",
              "2013-07.npz", "2013-08.npz", "2013-09.npz", "2013-10.npz", "2013-11.npz", "2013-12.npz", "2014-01.npz",
              "2014-02.npz", "2014-03.npz", "2014-04.npz", "2014-05.npz", "2014-06.npz", "2014-07.npz", "2014-08.npz"]

test_data = ["2014-09.npz", "2014-10.npz", "2014-11.npz", "2014-12.npz", "2015-01.npz", "2015-02.npz", "2015-03.npz",
             "2015-04.npz", "2015-05.npz", "2015-06.npz", "2015-07.npz", "2015-08.npz", "2015-09.npz", "2015-10.npz",
             "2015-11.npz", "2015-12.npz", "2016-01.npz", "2016-02.npz", "2016-03.npz", "2016-04.npz", "2016-05.npz",
             "2016-06.npz", "2016-07.npz", "2016-08.npz", "2016-09.npz", "2016-10.npz", "2016-11.npz", "2016-12.npz",
             "2017-01.npz", "2017-02.npz"]

expected_nodes = {"AOD", "CHLORA", "CLD_FR", "CLD_RD", "CLD_WP", "COT", "CO_M", "FIRE", "LAI", "LSTD",
                  "LSTD_AN", "LSTN", "LSTN_AN", "NO2", "NDVI", "OZONE", "SNOWC", "SST", "WV"}

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("full_dataset_dir", type=Path)
    parser.add_argument("output_path", type=Path)
    return parser.parse_args()

def main(args: Namespace):
    nodes = sorted(x.name for x in args.full_dataset_dir.iterdir() if x.is_dir())
    assert expected_nodes.difference(nodes) == set(), f"Not found: {expected_nodes.difference(nodes)}"
    logger.info(f"All found nodes: {nodes}")

    args.output_path.mkdir(exist_ok=True, parents=True)
    for node in tqdm(expected_nodes):
        (args.output_path / "train" / node).mkdir(parents=True)
        (args.output_path / "test" / node).mkdir(parents=True)
        node_files = [x.name for x in (args.full_dataset_dir / node).iterdir() if x.name in [*train_data, *test_data]]
        for item in node_files:
            out_file = args.output_path / ('train' if item in train_data else 'test') / node / item
            shutil.copyfile(f"{args.full_dataset_dir}/{node}/{item}", out_file)
        if node == "CO_M":
            shutil.copyfile(f"{args.full_dataset_dir}/CO_M/2009-07.npz", args.output_path / "train/CO_M/2009-08.npz")
            shutil.copyfile(f"{args.full_dataset_dir}/CO_M/2009-10.npz", args.output_path / "train/CO_M/2009-09.npz")
        if node == "SNOWC":
            shutil.copyfile(f"{args.full_dataset_dir}/SNOWC/2016-01.npz", args.output_path / "test/SNOWC/2016-02.npz")
    logger.info(f"Done splittig train-test data in '{args.output_path}'")

if __name__ == "__main__":
    main(get_args())
