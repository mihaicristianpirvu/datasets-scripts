#!/usr/bin/env python3
"""
Script to convert the raw png (grayscale) images from NEO dataset to numpy format (float32 [0:1])
Usage: python convert_png_to_npy.py /path/to/raw_dir /path/to/args.output_path
"""

from media_processing_lib.image import image_read, image_resize
from argparse import ArgumentParser, Namespace
from pathlib import Path
from tqdm import tqdm
import numpy as np
from functools import partial
from multiprocessing import Pool, cpu_count
from loguru import logger

def do_one(file: tuple[Path, Path], resolution: tuple[int, int]):
    assert len(resolution) == 2, resolution
    assert len(file) == 2 and file[0].suffix in (".PNG", ".png") and file[1].suffix == ".npz", file
    in_path, out_path = file
    out_path.parent.mkdir(exist_ok=True, parents=True)
    if out_path.exists():
        return

    image = image_read(in_path)
    resized_image = image_resize(image, height=resolution[0], width=resolution[1])[..., 0:1]
    # images: [0 : 255] as uint8
    float_image = np.float16(resized_image)
    # convert white pixels to nans
    float_image[float_image == 255] = np.nan
    float_image[~np.isnan(float_image)] /= 255
    np.savez(out_path, float_image)

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("raw_dir", type=lambda p: Path(p).absolute())
    parser.add_argument("--output_path", "-o", required=True, type=lambda p: Path(p).absolute())
    parser.add_argument("--resolution", nargs="+", type=int, default=[540, 1080])
    parser.add_argument("--n_cores", type=int, default=-1)
    args = parser.parse_args()
    assert len(args.resolution) == 2, f"Expected height and width, got {args.resolution}"
    return args

def main(args: Namespace):
    args = get_args()
    representations = [x.name for x in args.raw_dir.iterdir() if x.is_dir()]
    assert len(representations) > 0, "No representations found"
    logger.info(f"In path: '{args.raw_dir}'")
    logger.info(f"Out path: '{args.output_path}'")
    logger.info(f"Representations: {representations}")

    # gather each file and it's corresponding output path
    files: list[tuple[str, str]] = []
    for representation in representations:
        repr_files = [x for x in (args.raw_dir / representation).iterdir() if x.suffix in (".PNG", ".png")]
        assert len(repr_files) > 0, f"Representation '{args.raw_dir}/{representation}' has 0 files!"
        for repr_file in sorted(repr_files, key=lambda p: p.name):
            # MODAL2_M_AER_OD_2001-04.PNG => 2001-04
            out_name = args.output_path / representation / (repr_file.name.split(".")[0].split("_")[-1] + ".npz")
            files.append((repr_file.absolute(), out_name))
    logger.info(f"Gathered {len(files)} files to convert from raw png to npy")

    f = partial(do_one, resolution=args.resolution)
    map_fn = map if args.n_cores == 0 else (Pool(cpu_count() if args.n_cores == -1 else args.n_cores).imap)
    list(map_fn(f, tqdm(files)))

if __name__ == "__main__":
    main(get_args())
