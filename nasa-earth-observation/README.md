<b><h1>NEO dataset</h1></b>

Preprocessing and loading scripts for NEO datasets: 1 week and 1 month variants. Go in either [1 week](neo_1week) or
[1 month](neo_1month) directories.

Steps:
1. **Download raw data**

    Use the `./download_raw_data.py -o raw_data` script.

    1.1 **For weekly data only**. Use the `preprocess_raw_1week_data.py raw_data -o raw_data_fixed` script.

    - NDVI is only sampled at 16 days, we make an average of two consecutive samples to create an 8 day average.

    - We rename the data from YYYY-MM-DD to YYYY-MM-E (E=1,2,3,4 depending on which week bucket). This is needed because
        different sensors sample data at different weeks


2. **Convert from PNG to NPY files**
  - Converts from raw PNG to [0:1] normalied, 540x1080 npy files, and adds NaNs to invalid regions in the NEO data.

  `./convert_png_to_npy.py raw_data -o npy_data [--resolution 540 1080]`

   - If `--resolution` is not set, it will default to `540 x 1080`.

3. Run the [neo_viewer](neo_viewer.ipynb) and [data analysis](neo_data_analysis.ipynb) notebook
   - Set the path to `neo_1week/npy_data` or `neo_1month/npy_data`.

4. Use in ML python scripts via the [neo reader](neo_reader/neo_reader.py). You can also run it with the `npy_data`.
