#!/usr/bin/env python3
from pathlib import Path
from argparse import ArgumentParser
from datetime import timedelta, datetime
import numpy as np
import pandas as pd
from media_processing_lib.image import image_read, image_write
from functools import lru_cache
from multiprocessing import Pool, cpu_count
from tqdm import tqdm
from natsort import natsorted
import shutil

@lru_cache
def _read_img(path: Path) -> np.ndarray:
    return image_read(path).astype(int)

def _do_one_16_to_8(x: tuple[Path, Path, Path]):
    prev_img_path, curr_img_path, out_path = x
    if out_path.exists():
        return
    prev, curr = _read_img(prev_img_path), _read_img(curr_img_path)
    prev_mask = ((prev == [255, 255, 255]).sum(-1, keepdims=True) > 0)
    curr_mask = ((curr == [255, 255, 255]).sum(-1, keepdims=True) > 0)

    # common pixels: average where both masks are invalid (255 everywhere) or both images are valid
    cond_both = ~(prev_mask ^ curr_mask)
    cond_prev = ~prev_mask & curr_mask
    cond_curr = prev_mask & ~curr_mask
    new_img = ((prev + curr) / 2) * cond_both + prev * cond_prev + curr * cond_curr
    new_img = new_img.astype(np.uint8)
    image_write(new_img, out_path)

def fix_ndvi_16_to_8(in_dir: Path):
    """fixed ndvi_16 to be at an interval of 8 days. Averages two consecutive images"""
    ndvi_16_dir = in_dir / "NDVI_16"
    ndvi_8_dir = in_dir / "NDVI"
    if not ndvi_8_dir.exists():
        shutil.copytree(ndvi_16_dir, ndvi_8_dir)
    files = [x for x in ndvi_16_dir.iterdir() if x.suffix == ".png"]
    file_dates = sorted([datetime(*tuple(map(int, file.stem.split("-")))) for file in files])
    print(f"Converting NDVI_16 to NDVI_8. Got {len(files)} files")

    out_paths = []
    for i in range(1, len(files)):
        out_path = ndvi_8_dir / ((file_dates[i] - timedelta(days=8)).strftime("%Y-%m-%d") + ".png")
        out_paths.append(out_path)
    items = list(zip(files[:-1], files[1:], out_paths))

    # fn = map
    fn = Pool(processes=cpu_count() - 1).imap_unordered
    with tqdm(total=len(items)) as pbar:
        for _ in fn(_do_one_16_to_8, items):
            pbar.update()

def f_date_to_bucket(date: str) -> tuple[int, int, int]:
    year, month, bucket_id = int(date[0:4]), int(date[5:7]), 1 + (int(date[8:10]) - 1) // 8
    return (year, month, bucket_id)

def dist_to_bucket(date: str) -> int:
    """returns the distance to the closest bucket given an arbitrary date"""
    bucket = f_date_to_bucket(date)
    return abs(8 * (bucket[-1] - 1) + 4 - int(date[-2:]))

def print_diffs(data, date_to_bucket):
    items = {"before": {k: len(v) for k, v in data.items()}, "after": {k: len(v) for k, v in date_to_bucket.items()}}
    df = pd.DataFrame(items)
    df["diffs"] = df["before"] - df["after"]
    print(df)

def get_data_in_Es(in_dir: Path, representations: list[str]) -> dict[str, dict[str, str]]:
    print("Trying to convert the representations to a weekly interval based on days")
    data = {}
    for representation in representations:
        data[representation] = natsorted([f.stem for f in (in_dir / representation).iterdir() if f.suffix == ".png"])
        assert len(data[representation]) > 0, f"No files found for representation '{representation}'"

    bucket_to_date = {representation: {} for representation in representations}
    for representation in representations:
        for item in data[representation]:
            bucket = f_date_to_bucket(item)
            if bucket not in bucket_to_date[representation]:
                bucket_to_date[representation][bucket] = item
                continue
            # update existing bucket if the new item is closer to the bucket's center
            if dist_to_bucket(item) < dist_to_bucket(bucket_to_date[representation][bucket]):
                bucket_to_date[representation][bucket] = item
    date_to_bucket = {representation: {v: k for k, v in bucket_to_date[representation].items()}
                      for representation in representations}
    print_diffs(data, date_to_bucket)
    return date_to_bucket

def rename_and_export(in_dir: Path, date_to_bucket: dict[str, dict[str, str]], out_dir: Path):
    all_files = sum(len(x) for x in date_to_bucket.values())
    pbar = tqdm(total=all_files)
    for representation, repr_dtb in date_to_bucket.items():
        (out_dir / representation).mkdir(parents=True, exist_ok=True)
        pbar.set_description(f"Bucketing {representation}")
        for date, bucket in repr_dtb.items():
            pbar.update()
            in_file = in_dir / representation / (date + ".png")
            out_file = out_dir / representation / f"{bucket[0]}-{bucket[1]:02d}-E{bucket[2]}.png"
            if out_file.exists():
                continue
            shutil.copyfile(in_file, out_file)

def get_args():
    parser = ArgumentParser()
    parser.add_argument("in_dir", type=Path)
    parser.add_argument("--out_dir", "-o", type=Path)
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    fix_ndvi_16_to_8(args.in_dir)
    representations = [x.name for x in args.in_dir.iterdir() if x.is_dir()]
    assert "NDVI_16" in representations, "NDVI_16 not found. Only 1week export must use this script"
    representations.remove("NDVI_16")
    date_to_bucket = get_data_in_Es(args.in_dir, representations)
    rename_and_export(args.in_dir, date_to_bucket, args.out_dir)

if __name__ == "__main__":
    main()
